(ns leinproject.core
  (:gen-class))

(defn isEven []
  (println "Give me a number: ")
  (def number (read-string (read-line)))
  (cond
    (< number 0) (when true
                   (println "The number cannot be negative")
                   (recur))
    (even? number) (println "The number is even")
    :else (println "The number is odd")))

(defn printDivisibleBy3 [upperLimit]
  (doseq [i (range 1 (+ upperLimit 1)) :when (zero? (mod i 3))]
    (println i)))

(defn lottery []
  (set (take 7 (set (take 14 (repeatedly #(inc (rand-int 39))))))))

(defn syt [p,q]
  (cond (zero? q) p
        :else (recur q (mod p q))))

(defn -main
  [& args]

  (println "Exercises 1&2: ")
  (isEven)

  (println)
  (println "Exercise 3: ")
  (printDivisibleBy3 20)

  (println)
  (println "Exercise 4: ")
  (println(lottery))

  (println)
  (println "Exercise 5: ")
  (println(syt 102 68)))
