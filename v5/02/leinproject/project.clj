(defproject leinproject "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :plugins [[cider/cider-nrepl "0.21.1"]]
  :repl-options {:init-ns leinproject.core}
  :main leinproject.core)
