(ns test_lein.core
  (:gen-class))


(defn hogwarts [name] (str "Tervetuloa tylypahkaan " name))

(defn square [number] (* number number))

(def presidentti {:name {
                         :first "Urho"
                         :middle "Kaleva"
                         :last "Kekkonen"
                         }
                  })

(defn karkausvuosi [vuosi] 
  (cond (zero? (mod vuosi 400)) true
        (zero? (mod vuosi 100)) false
        (zero? (mod vuosi 4)) true
        :default false))

(defn -main
  [& args]
  (println (+ (* 2 5) 4))
  (println (+ 1 2 3 4 5))
  (println (hogwarts "Marko"))
  (println (square 3))
  (println (get (get presidentti :name) :middle))) 

(-main)
