const Kaara = (function() {
    const suojatut = new WeakMap();
    let tankki;

    class Kaara {
        constructor(p_tankki, p_matkamittari) {
            tankki = p_tankki;
            suojatut.set(this, {matkamittari: p_matkamittari});
        }

        aja() {
            if (tankki > 0) {
                console.log("Vroom!");
                suojatut.set(this, {matkamittari: this.getMittari() + 1});
                tankki--;
                console.log(this.getMittari() + " kilometriä ajettu");
                console.log("Bensaa jäljellä " + tankki + " litraa");
            } else console.log("Tankki tyhjä!")
            console.log(" ");
        }

        tankkaa(litraa) {
            console.log("Tankataan " + litraa + " litraa")
            tankki = tankki + litraa;
            console.log(" ")
        }

        getTankki() {
            return tankki;
        }

        getMittari() {
            return suojatut.get(this).matkamittari;
        }
    }
    return Kaara;
})();

const chevy = new Kaara(5, 0);
chevy.aja();
chevy.tankki = 50;
chevy.aja();
chevy.mittari = 0;
chevy.aja();
