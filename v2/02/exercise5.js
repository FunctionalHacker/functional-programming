var opiskelijat = [
	{nimi: 'Hermanni', ikä: 25, ala: 'Tietotekniikka'},
	{nimi: 'Petteri', ikä: 22, ala: 'Autotekniikka'},
	{nimi: 'Samppa', ikä: 35, ala: 'Tietotekniikka'},
	{nimi: 'Saara', ikä: 21, ala: 'Mediatekniikka'},
	{nimi: 'Panu', ikä: 22, ala: 'Tietotekniikka'}
];

var onNörtti = opiskelija => opiskelija.ala === 'Tietotekniikka'
var onVanha = opiskelija => opiskelija.ikä > 30

// lazy metodit arrayn läpikäyntiin
var filter = function* (suodata, lista) {
	for (var x of lista) {
		console.log('suodata', suodata(x), x.nimi);
		if (suodata(x))
			yield x;
	}
};

var find = (haku, lista) => {
	for (var x of lista) {
		console.log('haku', haku(x), x.nimi);
		if (haku(x))
			return x;
	}
};

// Etsitään ensimmäinen vanha nörtti listasta evaluoimatta koko listaa
var nörtit = filter(onNörtti, opiskelijat);
console.log(find(onVanha, nörtit));

// Tulostuksesta näkee, että koko listaa ei käydä läpi:
// suodata true Hermanni
// haku false Hermanni
// suodata false Petteri
// suodata true Samppa
// haku true Samppa
// { nimi: 'Samppa', 'ikä': 35, ala: 'Tietotekniikka' }
