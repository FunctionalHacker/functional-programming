var immutable = require('immutable')
const set1 = immutable.Set(['punainen', 'vihreä', 'keltainen']);
set2 = set1 & 'ruskea'
console.log(set1 === set2)
// palauttaa false
set3 = set2 & 'ruskea'
console.log(set2 === set3) 
// palauttaa true, eli ilmeisesti immutable settiin ei voi lisätä duplikaatteja
