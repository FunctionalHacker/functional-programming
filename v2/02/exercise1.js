function laskePisteet(lisapisteet){
	if(lisapisteet == 1.8) {
		return function (pituus) {
			if (pituus < 75) {
				return 60 + ((pituus - 75) * lisapisteet)
			}
			else if (pituus < 99) {
				return 60
			}
			else {
				return 60 + ((pituus - 99) * lisapisteet)
			}
		}
	}
	else if (lisapisteet == 2.0){
		return function(pituus) {
			if (pituus < 100) {
				return 60 + ((pituus - 100) * lisapisteet)
			}
			else if (pituus < 130) {
				return 60
			}
			else {
				return 60 + ((pituus - 130) * lisapisteet)
			}
		}
	}
}

function lahti(pituus){
	console.log("Lahden normaalimäki");
	console.log("Hypyn pituus: "+pituus+" metriä");
	console.log("Pisteet: "+laskePisteet(1.8)(pituus));
	console.log("");
}

function vantaa(pituus){
	console.log("Vantaan suurmäki");
	console.log("Hypyn pituus: "+pituus+" metriä");
	console.log("Pisteet: "+laskePisteet(2.0)(pituus));
	console.log("");
}

hypytLahti = [70,78,102,88,95,69,105];
hypytVantaa = [99,120,116,135,95,121,123];

for(i = 0; i < hypytLahti.length; i++){
	lahti(hypytLahti[i]);
}

for(i = 0; i < hypytVantaa.length; i++){
	vantaa(hypytVantaa[i]);
}
