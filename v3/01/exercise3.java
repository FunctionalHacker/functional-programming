public class Dicegame {
    public static void main(String ... args) {    	
    	long howMany6 = new Random().ints(20, 1, 7)
    			.filter(number -> number == 6)
    			.count();
    	System.out.println("Number of 6's: "+howMany6);
    }
}
