interface Convertable {
	double convert(double input);
}

public class Exercise1 {
	public static void main(String[] args){
		
		Convertable toCelsius = (fahrenheit) -> {
					return (5.0/9.0) * (fahrenheit-32);
		};
		System.out.println("78.0 fahrenheit is " + toCelsius.convert(78.0) + " celsius");
		
		Convertable area = (radius) -> {
			return Math.PI * radius * radius;
		};
		
		System.out.println("Area of a circle with a radius of 5cm is " + area.convert(5.0)+"cm^2");
	}
}
