// Transaktiot
public class PuttingIntoPractice{
    public static void main(String ...args){  
        List<Transaction> tr2012gt900 = transactions.stream()
        		.filter(transaction -> transaction.getYear() >= 2012)
        		.filter(transaction -> transaction.getValue() >= 900)
        		.collect(toList());
        
        System.out.println(tr2012gt900);
}

// Ruokalajit
public class Summarizing {

    public static void main(String ... args) {
		// En nähnyt järkeä käyttää map ja reduce functioita, koska on olemassa count
    	System.out.println("Number of dishes: "+menu.stream().count());
    }
}
