public class Words {
	public static void main(String ... args) throws IOException {
		Map<String, Integer> frequency = Files.lines(Paths.get("kalevala.txt"), Charset.defaultCharset())
			.flatMap(line -> Arrays.stream(line.split(" ")))
			.map(lines -> lines.replaceAll("/[,.!:]/g", ""))
			.collect(toMap(
						s -> s,
						s -> 1,
						Integer::sum));

		System.out.println(frequency);
	}
}
