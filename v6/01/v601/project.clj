(defproject v601 "0.1.0-SNAPSHOT"
  :plugins [[cider/cider-nrepl "0.21.1"]]
  :dependencies [[org.clojure/clojure "1.10.0"]]
  :repl-options {:init-ns v601.core}
  :main v601.core)
