import java.util.function.Function;
import java.util.function.UnaryOperator;

public class ChainOfResponsibility {

	public static void main(String[] args) {


		UnaryOperator<String> removeScandics =
			(String s) -> {
				return s.trim().replaceAll("ä", "a")
					.trim().replaceAll("Ä", "A")
					.trim().replaceAll("ö", "o")
					.trim().replaceAll("Ö", "O")
					.trim().replaceAll("å", "a")
					.trim().replaceAll("Å", "A");
			};

		UnaryOperator<String> removeSpaces =
			(String s) -> {
				return s.trim().replaceAll(" +", " ");
			};

		UnaryOperator<String> autoCorrect =
			(String s) -> {
				return s.trim().replaceAll("sturct", "struct");
			};



		Function<String, String> ketju = removeSpaces.andThen(removeScandics).andThen(autoCorrect); 

		String s = "Testailen  tässä      tätä chainofresponsibilityä sturct    öäåÅÄÖ sturct";
		s = ketju.apply(s);

		System.out.println(ketju.apply(s));
	}
}
