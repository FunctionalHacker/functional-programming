package AbstractFactory;
public abstract class Meijeri {
	public abstract Maito getMaito();
	public abstract Jogurtti getJogurtti();
	public abstract Juusto getJuusto();
}
