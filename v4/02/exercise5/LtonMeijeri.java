package AbstractFactory;

public class LtonMeijeri extends Meijeri {
	@Override
	public Maito getMaito() {
		return new LtonMaito();
	}

	@Override
	public Jogurtti getJogurtti() {
		return new LtonJogurtti();
	}

	@Override
	public Juusto getJuusto() {
		return new LtonJuusto();
	}
}
