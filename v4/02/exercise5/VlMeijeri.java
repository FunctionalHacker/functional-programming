package AbstractFactory;

public class VlMeijeri extends Meijeri {

	@Override
	public Maito getMaito() {
		return new VlMaito();
	}

	@Override
	public Jogurtti getJogurtti() {
		return new VlJogurtti();
	}

	@Override
	public Juusto getJuusto() {
		return new VlJuusto();
	}
}
