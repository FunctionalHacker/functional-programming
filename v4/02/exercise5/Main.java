package AbstractFactory;

public class Main {
	public static void main(String[] args) {
		Meijeri vlm = new VlMeijeri();
		Meijeri ltm = new LtonMeijeri();
		
		System.out.println(vlm.getMaito().toString());
		System.out.println(ltm.getMaito().toString());

		System.out.println(vlm.getJogurtti().toString());
		System.out.println(ltm.getJogurtti().toString());

		System.out.println(vlm.getJuusto().toString());
		System.out.println(ltm.getJuusto().toString());
	}
}
