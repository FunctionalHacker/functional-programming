Seuraava koodi kolminkertaistaa parilliset luvut ja summaa luvut yhteen.
```
import java.util.stream.IntStream;
public class StreamFilterMapReduce {
  public static void main(String[] args) {
  //  sum of the triples of even integers from 2 to 10
  System.out.printf(
    "Sum of the triples of even integers from 2 to 10 is: %d%n",
    IntStream.rangeClosed(1,10)
            .filter(x -> x%2 == 0)
            .map(x -> x*3)
            .sum());
  }
}
```
a) Kuinka monta kertaa filter-operaatio kutsuu lambdalauseketta?

10 kertaa

b) Kuinka monta kertaa map-operaatio kutsuu lambdalauseketta?

5 kertaa

c) Jos filter- ja map-operaatioiden järjestys vaihdetaan, kuinka monta kertaa map-operaatio kutsuu lambdalauseketta?

10 kertaa
