mport java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class Exerise5 {
	public static void main(String ... args) throws IOException {
		long startTime = System.currentTimeMillis();

		Map<String, Integer> frequencyParallel = Files.lines(Paths.get("kalevala.txt"), Charset.defaultCharset())
			.parallel()
			.flatMap(line -> Arrays.stream(line.split(" ")))
			.map(lines -> lines.replaceAll("/[,.!:]/g", ""))
			.collect(Collectors.toMap(
						s -> s,
						s -> 1,
						Integer::sum));

		long endTime = System.currentTimeMillis();
		long duration = (endTime - startTime);
		System.out.println("With parallel stream operation took: "+duration+"ms");

		startTime = System.currentTimeMillis();

		Map<String, Integer> frequency = Files.lines(Paths.get("kalevala.txt"), Charset.defaultCharset())
			.flatMap(line -> Arrays.stream(line.split(" ")))
			.map(lines -> lines.replaceAll("/[,.!:]/g", ""))
			.collect(Collectors.toMap(
						s -> s,
						s -> 1,
						Integer::sum));

		endTime = System.currentTimeMillis();
		duration = (endTime - startTime);
		System.out.println("Without parallel stream operation took: "+duration+"ms");

		// Parallel streamillä meni 92ms ja ei parallelilla 13ms. Ei ole ilmeisesti tässä kovin järkevää
		// käyttää parallel streamia. Koitin lisätä sitä eri kohtiinkin mutta lopputulos oli suurinpiirtein sama
	}
}
